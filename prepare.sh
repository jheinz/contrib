#!/usr/bin/env bash
set -e # 'e' quits the script on error, 'x' prints every command for debugging 


# TODO
# This script should 

TIMESTAMP=`date "+%Y-%m-%d_%H-%M-%S"`

# Check Parameter, exit if it is not set
# Expecting either 'symlink' or 'rollback'
case $1 in
    symlink)
        echo "INFO: Creating back up of contrib files and creating new symlinks ..."
        
        # Verify that contib_test exists
        if [ ! -d "${CONTRIB_TEST}" ]
        then
            echo "ERROR: The directory '${CONTRIB_TEST}' does not exist"
            exit 1
        fi
        
        # Set the script to run inside the CVMFS transaction
        CONTRIB_SCRIPT="${SCRIPT_DIR}/contrib_new_symlinks.sh"
        ;;
    rollback)
        echo "INFO: Restoring the state from the backup files ..."
        
        # Get latest backup
        BACKUP_FOLDER=`find ${CONTRIB_BACKUP} -maxdepth 1 | sort | tail -n 1`
        
        # Overwrite if a second parameter is given
        if [ -n "$2" ]
        then
            BACKUP_FOLDER="${CONTRIB_BACKUP}/${$2}"
        fi
        
        # Check if backup directory exists
        if [ ! -d "${BACKUP_FOLDER}" ]
        then
            echo "ERROR: The backup directory '${BACKUP_FOLDER}' does not exist"
            exit 1
        fi
        
        # Set the script to run inside the CVMFS transaction
        CONTRIB_SCRIPT="${SCRIPT_DIR}/contrib_rollback.sh"
        ;;
    *)
        # Default / no match
        echo "ERROR: No or wrong parameters."
        echo
        echo "Usage:"
        echo "  $0 symlink - Create backup and overwrite symlinks"
        echo "  $0 rollback - Rollback latest backup"
        echo "  $0 rollback 2019-01-30_19-10-52 - Rollback specified backup"
        exit 1
        ;;
esac

# Verify that script exists
if [ ! -f "${CONTRIB_SCRIPT}" ]
then
    echo "ERROR: The script 'contrib_new_symlinks.sh' or 'contrib_rollback.sh' does not exist or is not set"
    exit 1
fi


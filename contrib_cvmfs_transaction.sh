#!/usr/bin/env bash
set -e # 'e' quits the script on error, 'x' prints every command for debugging 

################################################################################################
#  This script manages the replacement of old symbolic links or files in the 'contrib'         #
#  directory on CVMFS. These can be replaced with a newer version that is stored under         #
#  'releases' by creating new symbolic links.                                                  #
#  This script can also be used to rollback to the previous state if something went wrong.     #
#  It must be run on the sft.cern.ch release manager machine. The script doesn't publish any   #
#  changes to CVMFS. This has to be done manually. Please follow the instructions that already #
#  printed after the transaction tasks are done.                                               #
#                                                                                              #
#  Created 2019-01-30 by Johannes Heinz (technical student)                                    #
#  Requirements: bash, date, cvmfs_server, find, sort, tail, cp, readlink, ln, basename, mkdir #
################################################################################################


# PART I: Prepare all parameters and check if the environment is as expected
################################################################################################

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# This script cannot load the other scripts as user cvsft if these are stored
# in the AFS area of user sftnight. Use a local directory like /tmp instead.
if [[ $SCRIPT_DIR == /afs/* ]]
then
    echo "ERROR: This script does not work if it is stored on AFS."
    echo "       Please use a local folder on the release manager machine instead."
    exit 1
fi

# Read the path and version number constants
source "${SCRIPT_DIR}/contrib_constants.sh"

# Check environment variables, exit if they are not set
echo
echo "------------------------------------------------------------------------------------------"
echo "CONTRIB:        ${CONTRIB:?You need to set CONTRIB (non-empty)}"
echo "CONTRIB_TEST:   ${CONTRIB_TEST:?You need to set CONTRIB_TEST (non-empty)}"
echo "CONTRIB_BACKUP: ${CONTRIB_BACKUP:?You need to set CONTRIB_BACKUP (non-empty)}"
echo "------------------------------------------------------------------------------------------"
echo

# Read parameters, define the scripts and the environment
source "${SCRIPT_DIR}/prepare.sh"

# PART II: Start the CVMFS transaction as user 'cvsft'
# (!) Escape all internal ENVs with a backslash, i.e. '\$?'
# (!) Do not escape the ENVs that are passed from outside, i.e. '$WORKSPACE'
################################################################################################
sudo -i -u cvsft<<EOF
# set -x
shopt -s nocasematch
for iteration in {1..25}
do
    cvmfs_server transaction sft.cern.ch
    if [ \$? -ne 0 ]
    then
        if  [[ "\${iteration}" == "25" ]]
        then
            echo "Too many tries. Aborting ... "
            exit 1
        else
            echo "Another CVMFS transaction is already open."
            echo "Going to sleep for 5 minutes ..."
            echo ""
            sleep 5m
        fi
    else
        # Transaction was opened successfully
        break
    fi
done

# Pass all ENVs manually since they are executed by a different user
TIMESTAMP="${TIMESTAMP}" \
BACKUP_FOLDER="${BACKUP_FOLDER}" \
${CONTRIB_SCRIPT}

# Evalulate the result and either publish or rollback all changes
UPDATE_RESULT=\$?
if [ \$UPDATE_RESULT -eq 0 ]
then
    cd \$HOME
    echo "######################################################################"
    echo "##                                                                  ##"
    echo "##          ATTENTION! USER INTERACTION IS REQUIRED NOW!            ##"
    echo "##                                                                  ##"
    echo "##  The process finished successfully. Please verify that the state ##"
    echo "##  of all directories is as desired:                               ##"
    echo "##                                                                  ##"
    echo "##          ./contrib_info.sh                                       ##"
    echo "##                                                                  ##"
    echo "##  Afterwards, you can publish the changes to CVMFS:               ##"
    echo "##                                                                  ##"
    echo "##          sudo -i -u cvsft                                        ##"
    echo "##          cvmfs_server publish sft.cern.ch                        ##"
    echo "##                                                                  ##"
    echo "##  If this was just a test run or you detected a corrupt state,    ##
    echo "##  you should abort the CVMFS transaction and rollback:            ##"
    echo "##                                                                  ##"
    echo "##          sudo -i -u cvsft                                        ##"
    echo "##          cvmfs_server abort -f sft.cern.ch                       ##"
    echo "##                                                                  ##"
    echo "######################################################################"
else
    echo
    echo "The operations in CVMFS failed. Aborting the transaction ..."
    echo
    cd \$HOME
    cvmfs_server abort -f sft.cern.ch
    exit 1
fi
EOF


#!/usr/bin/env bash
set -e # 'e' quits the script on error, 'x' prints every command for debugging 

#####################################################################################
#  This script creates a backup of the current state of the 'contrib' folder in     #
#  cvmfs - but only where we are making changes (preserving properties).            #
#  Then it sets the new symlinks for 'gcc', 'clang' and 'binutils'                  #
#                                                                                   #
#  Created 2019-01-30 by Johannes Heinz (technical student)                         #
#  Requirements: bash, cp, dirname, pwd, readlink, ln, basename, mkdir              #
#####################################################################################


# Read the path and version number constants
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source "${SCRIPT_DIR}/contrib_constants.sh"

# Check environment variables, exit if they are not set
echo
echo "------------------------------------------------------------------------------------------"
echo "CONTRIB:             ${CONTRIB:?You need to set CONTRIB (non-empty)}"
echo "CONTRIB_TEST:        ${CONTRIB_TEST:?You need to set CONTRIB_TEST (non-empty)}"
echo "CONTRIB_BACKUP:      ${CONTRIB_BACKUP:?You need to set CONTRIB_BACKUP (non-empty)}"
echo "------------------------------------------------------------------------------------------"
echo "BINUTILS_VERSIONS:   ${BINUTILS_VERSIONS[@]:?You need to set BINUTILS_VERSIONS (non-empty)}"
echo "CLANG_VERSIONS:      ${CLANG_VERSIONS[@]:?You need to set CLANG_VERSIONS (non-empty)}"
echo "GCC_VERSIONS:        ${GCC_VERSIONS[@]:?You need to set GCC_VERSIONS (non-empty)}"
echo "------------------------------------------------------------------------------------------"
echo "TIMESTAMP:           ${TIMESTAMP:?You need to set TIMESTAMP (non-empty)}"
echo "------------------------------------------------------------------------------------------"
echo "REAL_CLANG_VERSIONS: ${REAL_CLANG_VERSIONS[@]:?You need to set REAL_CLANG_VERSIONS (non-empty)}"
echo "REAL_GCC_VERSIONS:   ${REAL_GCC_VERSIONS[@]:?You need to set REAL_GCC_VERSIONS (non-empty)}"
echo "REAL_GCC_PLATFORMS:  ${REAL_GCC_PLATFORMS[@]:?You need to set REAL_GCC_PLATFORMS (non-empty)}"
echo "LLVM_ALIAS:          ${LLVM_ALIAS[@]:?You need to set LLVM_ALIAS (non-empty)}"
echo "GCC62_ALIAS:         ${GCC62_ALIAS[@]:?You need to set GCC62_ALIAS (non-empty)}"
echo "GCC7_ALIAS:          ${GCC7_ALIAS[@]:?You need to set GCC7_ALIAS (non-empty)}"
echo "GCC81_ALIAS:         ${GCC81_ALIAS[@]:?You need to set GCC81_ALIAS (non-empty)}"
echo "GCC82_ALIAS:         ${GCC82_ALIAS[@]:?You need to set GCC82_ALIAS (non-empty)}"
echo "------------------------------------------------------------------------------------------"
echo

# Create the backup folder
mkdir -p ${CONTRIB_BACKUP}/${TIMESTAMP}


# [FUNCTIONS]
# -------------------------------------------------------------------------------------------------

# Backup old files if necessary; Expects the following parameters:
# - A subpath (e.g. 'binutils')
# - An list of versions (e.g. '${BINUTILS_VERSIONS[@]}')
function contrib_backup() {
    echo "BACKUP of $1 ..."; echo
    local SOURCE="${CONTRIB}/$1"
    local TARGET="${CONTRIB_BACKUP}/${TIMESTAMP}/$1"
    shift
    local VERSIONS_ARRAY=("$@")
    
    mkdir ${TARGET}
    for VERSION in ${VERSIONS_ARRAY[@]}
    do
        if [[ -L ${SOURCE}/${VERSION} || -e ${SOURCE}/${VERSION} ]]
        then
            set -x
            cp --archive ${SOURCE}/${VERSION} ${TARGET}
            { set +x; } 2>/dev/null
        fi
    done
    echo
    echo
}

# Remove old files if necessary; Expects the following parameters:
# - A subpath (e.g. 'binutils')
# - An list of versions (e.g. '${BINUTILS_VERSIONS[@]}')
function contrib_remove() {
    echo "REMOVAL of $1 ..."; echo
    local SOURCE="${CONTRIB}/$1"
    shift
    local VERSIONS_ARRAY=("$@")
    
    for VERSION in ${VERSIONS_ARRAY[@]}
    do
        if [[ -L ${SOURCE}/${VERSION} || -e ${SOURCE}/${VERSION} ]]
        then
            set -x
            rm -rf ${SOURCE}/${VERSION}
            { set +x; } 2>/dev/null
        else
            echo "'${SOURCE}/${VERSION}' doesn't exist"
        fi
    done
    echo
    echo
}

# Creates a symlink to support a wide variety of platform names; Expects the following parameters:
# - The compiler name (e.g. 'gcc')
# - The compiler version (e.g. '6.2.0')
# - A version suffix for the symlinks (w.g. 'gcc62-opt')
function contrib_symlink_platform() {
    pushd ${CONTRIB}/${1}/${2}
    for PLATFORM in *
    do
        local LINKED_PLATFORM="${PLATFORM}-${3}"
        set -x
        ln -s ${PLATFORM} ${LINKED_PLATFORM}
        { set +x; } 2>/dev/null
    done
    popd
}

# Creates a symlink to support a wide variety of version names; Expects the following parameters:
# - The compiler name (e.g. 'gcc')
# - The compiler version (e.g. '6.2.0')
# - An list of alias versions (e.g. '${GCC62_ALIAS[@]}')
function contrib_symlink_version() {
    local TARGET="${CONTRIB}/$1"
    shift
    local ORIGINAL="$1"
    shift
    local ALIAS_ARRAY=("$@")
    
    pushd ${TARGET}
    for ALIAS in ${ALIAS_ARRAY[@]}
    do
        set -x
        ln -s ${ORIGINAL} ${ALIAS}
        { set +x; } 2>/dev/null
    done
    popd
}

# Verify installation of symbolic links; Expects the following parameters:
# - A subpath (e.g. 'binutils')
# - An list of versions (e.g. '${BINUTILS_VERSIONS[@]}')
function contrib_verify() {
    echo "VERIFICATION of $1 ..."; echo
    local SOURCE="${CONTRIB}/$1"
    shift
    local VERSIONS_ARRAY=("$@")
    
    for VERSION in ${VERSIONS_ARRAY[@]}
    do
        if [ -e ${SOURCE}/${VERSION} ]
        then
            for ITEM in ${SOURCE}/${VERSION}/*
            do
                if [ -L ${ITEM} ]
                then
                    if [ -e ${ITEM} ]
                    then
                        echo "'$ITEM' is a valid symlink"
                    else
                        echo "ERROR: '$ITEM' is a broken symlink"
                        exit 1
                    fi
                elif [ -e ${ITEM} ]
                then
                    echo "ERROR: '$ITEM' is a not symlink"
                    exit 1
                else
                    echo "ERROR: '$ITEM' does not exist"
                    exit 1
                fi
            done
        else
            echo "ERROR: '${SOURCE}/${VERSION}' does not exist"
            exit 1
        fi
    done
    echo
}


# [BACKUP]
# -------------------------------------------------------------------------------------------------
contrib_backup "binutils" "${BINUTILS_VERSIONS[@]}"
contrib_backup "clang" "${CLANG_VERSIONS[@]}"
contrib_backup "llvm" "${CLANG_VERSIONS[@]}"
contrib_backup "gcc" "${GCC_VERSIONS[@]}"
echo "------------------------------------------------------------------------------------------"
echo
echo


# [REMOVAL]
# -------------------------------------------------------------------------------------------------
contrib_remove "binutils" "${BINUTILS_VERSIONS[@]}"
contrib_remove "clang" "${CLANG_VERSIONS[@]}"
contrib_remove "llvm" "${CLANG_VERSIONS[@]}"
contrib_remove "gcc" "${GCC_VERSIONS[@]}"
echo "------------------------------------------------------------------------------------------"
echo
echo


# [REWRITE] binutils
# -------------------------------------------------------------------------------------------------
echo "REWRITE of binutils ..."
echo
for BINUTIL_VERSION in ${CONTRIB_TEST}/binutils/*
do
    BINUTIL_VERSION_BASE="`basename ${BINUTIL_VERSION}`"
    echo " - Rewriting version ${BINUTIL_VERSION_BASE} ..."
    
    TARGET="${CONTRIB}/binutils/${BINUTIL_VERSION_BASE}"
    mkdir -p ${TARGET}
    
    for BINUTIL_PLATFORM in ${BINUTIL_VERSION}/*
    do
        ln -s $(readlink -f ${BINUTIL_PLATFORM}) ${TARGET}
    done
done
echo
echo


# [REWRITE] clang
# -------------------------------------------------------------------------------------------------
echo "REWRITE of clang ..."
echo
for CLANG_VERSION_BASE in ${REAL_CLANG_VERSIONS[@]}
do
    SOURCE="${CONTRIB_TEST}/clang/${CLANG_VERSION_BASE}"
    TARGET="${CONTRIB}/clang/${CLANG_VERSION_BASE}"
    
    echo " - Rewriting version ${CLANG_VERSION_BASE} ..."
    mkdir -p ${TARGET}
    
    for CLANG_PLATFORM in ${SOURCE}/*
    do
        ln -s $(readlink -f ${CLANG_PLATFORM}) ${TARGET}
    done
done

# External symbolic links for versions
echo " - Linking versions ..."
contrib_symlink_version "clang" "6.0.0" "6.0.0binutils"
contrib_symlink_version "clang" "7.0.0" "7.0.0binutils"
echo
echo


# [REWRITE] llvm
# -------------------------------------------------------------------------------------------------
echo "REWRITE of llvm ..."
echo
# Special case: The old name for 'clang' is 'llvm'
echo " - Linking LLVM to clang ..."
pushd "${CONTRIB}/llvm"
for ALIAS in ${LLVM_ALIAS[@]}
do
    set -x
    ln -s ../clang/${ALIAS} ${ALIAS}
    { set +x; } 2>/dev/null
done
popd
echo
echo


# [REWRITE] gcc
# -------------------------------------------------------------------------------------------------
echo "REWRITE of gcc ..."
echo
for GCC_VERSION_BASE in ${REAL_GCC_VERSIONS[@]}
do    
    SOURCE="${CONTRIB_TEST}/gcc/${GCC_VERSION_BASE}"
    TARGET="${CONTRIB}/gcc/${GCC_VERSION_BASE}"
    
    echo " - Rewriting version ${GCC_VERSION_BASE} ..."
    mkdir -p ${TARGET}
    
    for GCC_PLATFORM_BASE in ${REAL_GCC_PLATFORMS[@]}
    do
        GCC_PLATFORM="${SOURCE}/${GCC_PLATFORM_BASE}"
        ln -s $(readlink -f ${GCC_PLATFORM}) ${TARGET}
    done
done

# Internal symbolic links for platforms
echo " - Linking platforms ..."
contrib_symlink_platform "gcc" "6.2.0" "gcc62-opt"
contrib_symlink_platform "gcc" "7.3.0" "gcc7-opt"
contrib_symlink_platform "gcc" "8.1.0" "gcc8-opt"
contrib_symlink_platform "gcc" "8.2.0" "gcc8-opt"

# External symbolic links for versions
echo " - Linking versions ..."
contrib_symlink_version "gcc" "6.2.0" "${GCC62_ALIAS[@]}"
contrib_symlink_version "gcc" "7.3.0" "${GCC7_ALIAS[@]}"
contrib_symlink_version "gcc" "8.1.0" "${GCC81_ALIAS[@]}"
contrib_symlink_version "gcc" "8.2.0" "${GCC82_ALIAS[@]}"
echo
echo
echo "------------------------------------------------------------------------------------------"
echo
echo


# [VERIFICATION]
# -------------------------------------------------------------------------------------------------
contrib_verify "binutils" "${BINUTILS_VERSIONS[@]}"
echo
contrib_verify "clang" "${CLANG_VERSIONS[@]}"
echo
contrib_verify "llvm" "${CLANG_VERSIONS[@]}"
echo
contrib_verify "gcc" "${GCC_VERSIONS[@]}"


#!/usr/bin/env bash
set -e # 'e' quits the script on error, 'x' prints every command for debugging 

#####################################################################################
#  This script shows the current state of all relevant contrib folders.             #
#                                                                                   #
#  Created 2019-02-05 by Johannes Heinz (technical student)                         #
#  Requirements: bash, find, sort, tail, dirname, basename, cut, sed, pwd           #
#####################################################################################

# Read the path and version number constants
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source "${SCRIPT_DIR}/contrib_constants.sh"

# Check environment variables, exit if they are not set
echo
echo "------------------------------------------------------------------------------------------"
echo "CONTRIB:           ${CONTRIB:?You need to set CONTRIB (non-empty)}"
echo "CONTRIB_TEST:      ${CONTRIB_TEST:?You need to set CONTRIB_TEST (non-empty)}"
echo "CONTRIB_BACKUP:    ${CONTRIB_BACKUP:?You need to set CONTRIB_BACKUP (non-empty)}"
echo "------------------------------------------------------------------------------------------"
echo "BINUTILS_VERSIONS: ${BINUTILS_VERSIONS:?You need to set BINUTILS_VERSIONS (non-empty)}"
echo "CLANG_VERSIONS:    ${CLANG_VERSIONS:?You need to set CLANG_VERSIONS (non-empty)}"
echo "GCC_VERSIONS:      ${GCC_VERSIONS:?You need to set GCC_VERSIONS (non-empty)}"
echo "------------------------------------------------------------------------------------------"
echo


# List files in compiler directories; Expects the following parameters:
# - An absolute path to a compiler in a contrib directory (e.g. '/cvmfs/sft.cern.ch/lcg/contrib/binutils')
# - An list of versions (e.g. '${BINUTILS_VERSIONS[@]}')
function compiler_info() {
    local SOURCE="$1"
    shift
    local VERSIONS_ARRAY=("$@")
    
    for VERSION in ${VERSIONS_ARRAY[@]}
    do
        if [ -e "${SOURCE}/${VERSION}" ]
        then
            echo "Listing '${SOURCE}/${VERSION}':"
            ls -hl ${SOURCE}/${VERSION}
            echo
        else
            echo "INFO: '${SOURCE}/${VERSION}' does not exist"
        fi
    done
    echo
}

# List all compilers inside a given contrib directory
# - An absolute path to a contrib directory (e.g. '/cvmfs/sft.cern.ch/lcg/contrib_test')
function contrib_info() {
    echo "__Binutils__________________________________________________________________________________"
    compiler_info "${1}/binutils" "${BINUTILS_VERSIONS[@]}"
    echo "__Clang_____________________________________________________________________________________"
    compiler_info "${1}/clang" "${CLANG_VERSIONS[@]}"
    echo "__LLVM______________________________________________________________________________________"
    compiler_info "${1}/llvm" "${CLANG_VERSIONS[@]}"
    echo "__GCC_______________________________________________________________________________________"
    compiler_info "${1}/gcc" "${GCC_VERSIONS[@]}"
}


echo
echo "============================================================================================"
echo "[CONTRIB]"
echo "============================================================================================"
contrib_info "${CONTRIB}"

echo
echo "============================================================================================"
echo "[CONTRIB_TEST]"
echo "============================================================================================"
contrib_info "${CONTRIB_TEST}"

echo
echo "============================================================================================"
echo "[CONTRIB_BACKUP]"
echo "============================================================================================"
if [ -d "${CONTRIB_BACKUP}" ]
then
    # Get latest backup timestamp
    BACKUP_FOLDER=$( find ${CONTRIB_BACKUP} -maxdepth 1 | sort | tail -n 1 )
    BACKUP_DATE=$( basename ${BACKUP_FOLDER} | cut -d "_" -f 1 )
    BACKUP_TIME=$( basename ${BACKUP_FOLDER} | cut -d "_" -f 2 | sed -e 's/-/:/g' )
    
    echo "[${BACKUP_DATE} ${BACKUP_TIME}]"
    echo "============================================================================================"
    
    contrib_info "${BACKUP_FOLDER}"
else
    echo "INFO: '${CONTRIB_BACKUP}' does not exist. Nothing to show."
fi
echo


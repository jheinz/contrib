#!/usr/bin/env bash
set -e # 'e' quits the script on error, 'x' prints every command for debugging 

#####################################################################################
#  This script copies the backup of a preious state of the 'contrib' folder in      #
#  cvmfs back to its original place ('gcc', 'clang' and 'binutils')                 #
#                                                                                   #
#  Created 2019-01-30 by Johannes Heinz (technical student)                         #
#  Requirements: bash, cp, rm, dirname, pwd                                         #
#####################################################################################


# Read the path and version number constants
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source "${SCRIPT_DIR}/contrib_constants.sh"

# Check environment variables, exit if they are not set
echo
echo "------------------------------------------------------------------------------------------"
echo "CONTRIB:           ${CONTRIB:?You need to set CONTRIB (non-empty)}"
echo "BACKUP_FOLDER:     ${BACKUP_FOLDER:?You need to set BACKUP_FOLDER (non-empty)}"
echo "------------------------------------------------------------------------------------------"
echo "BINUTILS_VERSIONS: ${BINUTILS_VERSIONS:?You need to set BINUTILS_VERSIONS (non-empty)}"
echo "CLANG_VERSIONS:    ${CLANG_VERSIONS:?You need to set CLANG_VERSIONS (non-empty)}"
echo "GCC_VERSIONS:      ${GCC_VERSIONS:?You need to set GCC_VERSIONS (non-empty)}"
echo "------------------------------------------------------------------------------------------"
echo


# Remove new symlinks in 'contrib' and restore the previously saved backup; Expects the following parameters:
# - A subpath (e.g. 'binutils')
# - An list of versions (e.g. '${BINUTILS_VERSIONS[@]}')
function contrib_remove_restore() {
    local COMPILER="$1"
    shift
    local VERSIONS_ARRAY=("$@")
    
    echo "REMOVAL of new ${COMPILER} ..."
    for VERSION in ${VERSIONS_ARRAY[@]}
    do
        set -x
        rm -rf ${CONTRIB}/${COMPILER}/${VERSION}
        { set +x; } 2>/dev/null
    done
    echo
    echo
    echo "RESTORATION of old ${COMPILER} ..."
    cp --archive ${BACKUP_FOLDER}/${COMPILER}/* ${CONTRIB}/${COMPILER}
    echo
}


# [REMOVAL & RESTORATION]
# -------------------------------------------------------------------
contrib_remove_restore "binutils" "${BINUTILS_VERSIONS[@]}"
echo
contrib_remove_restore "clang" "${CLANG_VERSIONS[@]}"
echo
contrib_remove_restore "llvm" "${CLANG_VERSIONS[@]}"
echo
contrib_remove_restore "gcc" "${GCC_VERSIONS[@]}"


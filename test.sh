#!/usr/bin/env bash
set -e # 'e' quits the script on error, 'x' prints every command for debugging 

################################################################################################
#  This script allows the test of the script 'contrib_cvmfs_transaction.sh' and acts as close  #
#  as possible to this original. Therefore it sets up an overlay file system to avoid changes  #
#  in the real CVMFS.
#                                                                                              #
#  Created 2019-02-06 by Johannes Heinz (technical student)                                    #
#  Requirements: bash, date, cvmfs_server, find, sort, tail, cp, readlink, ln, basename, mkdir #
################################################################################################


SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# Set up the environment, fix the paths, mount the overlay file system
function setup_contrib_test() {
    echo "Setup ..."
    mkdir -p /tmp/overlayfs/{upper,work,merged}
    sudo mount -t overlay overlay -o lowerdir=/cvmfs/sft.cern.ch,upperdir=/tmp/overlayfs/upper,workdir=/tmp/overlayfs/work /tmp/overlayfs/merged
    sed -i "s/cvmfs\/sft.cern.ch/tmp\/overlayfs\/merged/" "${SCRIPT_DIR}/contrib_constants.sh"   
    source "${SCRIPT_DIR}/contrib_constants.sh"
    
    # Clean up automatically if any EXIT signal like 'exit 1' is triggered
    trap cleanup_contrib_test EXIT
}

# Clean up the overlay file system, reset the PATHs to cvmfs
function cleanup_contrib_test() {
    echo "Cleanup ..."
    sed -i "s/tmp\/overlayfs\/merged/cvmfs\/sft.cern.ch/" "${SCRIPT_DIR}/contrib_constants.sh"
    sudo umount overlay
    if [[ $REMOVE_OVERLAY == true ]]
    then
        echo "Removing overlay data ..."
        rm -rf /tmp/overlayfs
    fi
    echo
}

echo
echo "[ OVERLAY FILE SYSTEM ] Testing locally instead of using CVMFS ... "
echo

KERNEL_RELEASE=$( uname --kernel-release )
if [[ ! ${KERNEL_RELEASE} == 3.1* && ! ${KERNEL_RELEASE} == 4.* ]]
then
    echo "ERROR: The Linux kernel version is too old for overlayfs"
    exit 1
fi

# Set up PATHS, overlay file system
setup_contrib_test

# Check environment variables, exit if they are not set
echo
echo "------------------------------------------------------------------------------------------"
echo "CONTRIB:        ${CONTRIB:?You need to set CONTRIB (non-empty)}"
echo "CONTRIB_TEST:   ${CONTRIB_TEST:?You need to set CONTRIB_TEST (non-empty)}"
echo "CONTRIB_BACKUP: ${CONTRIB_BACKUP:?You need to set CONTRIB_BACKUP (non-empty)}"
echo "------------------------------------------------------------------------------------------"
echo

if [[ $1 == info ]]
then
    # Show info
    source "${SCRIPT_DIR}/contrib_info.sh"
    exit 0
elif [[ $1 == rm ]]
then
    # Remove overlay data
    REMOVE_OVERLAY="true"
    exit 0
else
    # Read parameters, define the scripts and the environment
    source "${SCRIPT_DIR}/prepare.sh"
    echo "[Simulation] cvmfs_server transaction sft.cern.ch"

    # Pass all ENVs manually
    TIMESTAMP="${TIMESTAMP}" \
    BACKUP_FOLDER="${BACKUP_FOLDER}" \
    ${CONTRIB_SCRIPT}

    # Evalulate the result and either publish or rollback all changes
    if [ $? -eq 0 ]
    then
        echo
        echo "The process finished successfully. Publishing the transaction ..."
        echo "[Simulation] cvmfs_server publish sft.cern.ch"
        exit 0
    else
        echo
        echo "The operations in CVMFS failed. Aborting the transaction ..."
        echo "[Simulation] cvmfs_server abort -f sft.cern.ch"
        exit 1
    fi
fi

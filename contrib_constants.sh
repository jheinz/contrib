#!/usr/bin/env bash
# This is a collection of paths and version numbers that are used in multiple scripts.

# Absolute paths to all relevant directories in CVMFS
CONTRIB="/cvmfs/sft.cern.ch/lcg/contrib"
CONTRIB_TEST="/cvmfs/sft.cern.ch/lcg/contrib_test"
CONTRIB_BACKUP="/cvmfs/sft.cern.ch/lcg/contrib_backup"

# All versions that are part of the renewal process
BINUTILS_VERSIONS=( 2.28  2.30 )
CLANG_VERSIONS=( 6.0.0  6.0.0binutils  7.0.0  7.0.0binutils )
GCC_VERSIONS=( 6.2  6.2binutils  6.2.0  6.2.0binutils  7  7binutils  7.3.0  7.3.0binutils  8  8binutils  8.1.0  8.1.0binutils  8.2.0  8.2.0binutils )

# Defining the real compilers and their aliases
REAL_CLANG_VERSIONS=( 6.0.0  7.0.0 )
LLVM_ALIAS=( 6.0.0  6.0.0binutils  7.0.0  7.0.0binutils )

REAL_GCC_VERSIONS=( 6.2.0  7.3.0  8.1.0  8.2.0 )
REAL_GCC_PLATFORMS=( x86_64-slc6  x86_64-centos7 )

GCC62_ALIAS=( 6.2  6.2binutils  6.2.0binutils )
GCC7_ALIAS=( 7  7binutils  7.3.0binutils )
GCC81_ALIAS=( 8.1.0binutils )
GCC82_ALIAS=( 8  8binutils  8.2.0binutils )

echo "Reading constants ..."


# Contrib

The script `contrib_cvmfs_transaction.sh` allows to 
 - backup and set the new compilers in `contrib` in CVMFS
 - restore the backup

The script `contrib_info.sh` allows to
 - show the current state of `contrib`, `contrib_test` and `contrib_backup` in CVMFS

The script `test.sh` allows to
 - test the features of `contrib_cvmfs_transaction.sh`
 - show the info about the overlay file system similar to `contrib_info.sh`
 - remove temporary files that are used in the overlay

**Attention:**
This script doesn't publish automatically.
This has to be done manually after the script has finished successfully to avoid the publication of a corrupt repository state.


## Single source of truth

For each compiler on each platform there is a [single source of truth](https://en.wikipedia.org/wiki/Single_source_of_truth). That means that all aliases like e.g the suffix `-gcc8-opt` for the platform or the suffix `binutils` for the version are just relative symbolic links to the *original* absolute symbolic link inside the `contrib` folder. Right now, there are the following *real* compilers:

**Binutils:**

 - `binutils/2.28/x86_64-centos7`
 - `binutils/2.28/x86_64-slc6`
 - `binutils/2.30/x86_64-centos7`
 - `binutils/2.30/x86_64-slc6`

**Clang:**

 - `clang/6.0.0/x86_64-centos7`
 - `clang/6.0.0/x86_64-slc6`
 - `clang/7.0.0/x86_64-centos7`
 - `clang/7.0.0/x86_64-slc6`

**GCC:**

 - `gcc/6.2.0/x86_64-centos7`
 - `gcc/6.2.0/x86_64-slc6`
 - `gcc/7.3.0/x86_64-centos7`
 - `gcc/7.3.0/x86_64-slc6`
 - `gcc/8.1.0/x86_64-centos7`
 - `gcc/8.1.0/x86_64-slc6`
 - `gcc/8.2.0/x86_64-centos7`
 - `gcc/8.2.0/x86_64-slc6`


## Testing

For testing, there is a script called `test.sh` that relies on [overlayfs](https://wiki.archlinux.org/index.php/Overlay_filesystem) to create a virtual layer on top of the read-only CVMFS repository `/cvmfs/sft.cern.ch`.
The advantage of this approach is that a release manager is not blocked during tests.

For these tests it is important to use at least a **CentOS 7** operating system since overlayfs is a rather new Linux kernel feature. There is no extra installation required but the user running the script needs to have `sudo` access.
That's why these tests will run neither on the release manager (it runs SLC6) nor on lxplus7 (no `sudo` for `sftnight`) nor on the build machines (no `sudo` for `sftnight`).

The solution is to run the test **locally** as long as you have a CVMFS client installed and configured or to use a VM on [**OpenStack**](https://openstack.cern.ch/project/instances/) for that.

```bash
# Create backup and new symbolic links
./test.sh symlink
# Show state of 'contrib', 'contrib_test' and 'contrib_backup' after new symlinks
./test.sh info
# Rollback latest backup
./test.sh rollback
# Show state of 'contrib', 'contrib_test' and 'contrib_backup' after rollback
./test.sh info
# Remove temporary files in the overlay file system directories
./test.sh rm
```

If you prefer to run a test on CVMFS (while blocking the release manager for other jobs), you can apply all parameters as mentioned in the section below.
As long as you abort the CVMFS transaction afterwards, your changes won't be applied to the CVMFS.


## Production

For the application of the scripts in the production system, it is important to get all scripts in this folder on the CVMFS release manager machine for the repository `sft.cern.ch` called `cvmfs-sft.cern.ch`.
The scripts **must not be stored on AFS** (e.g. the `$HOME` directory of user `sftnight`), use a local directory like `/opt` or `/tmp` instead. The reason for that is that the scripts need to be sourced by user `cvsft` who cannot access another user's AFS directory.


### Show the current state

```bash
./contrib_info.sh
```

### Create new symlinks

```bash
./contrib_cvmfs_transaction.sh symlink
```

### Restore the backup

**Latest backup:**
```bash
./contrib_cvmfs_transaction.sh rollback
```

**Specific backup:**
```bash
./contrib_cvmfs_transaction.sh rollback 2019-01-30_19-10-52
```
